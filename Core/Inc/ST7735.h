/*
 * ST7735.h
 *
 *  Created on: Dec 9, 2022
 *      Author: n0toose
 */

#ifndef __ST7735_H__
#define __ST7735_H__

#include "stm32f4xx_hal.h"
#include <stdbool.h>

// SPI Handle
extern SPI_HandleTypeDef hspi1;
#define ST7735_SPI_PORT hspi1

/* Pin Definitions (I change them around very often.) */
#define DC_PORT GPIOA
#define DC_PIN GPIO_PIN_6
#define RST_PORT GPIOD
#define RST_PIN GPIO_PIN_14
#define CS_PORT GPIOD
#define CS_PIN GPIO_PIN_15

/* TFT Definitions */
#define ST7735_WIDTH  128
#define ST7735_HEIGHT 160
// Lower than how other libraries define it.
#define DELAY 50 // milliseconds
// 16-bit color, see 10.1.7 (RDDCOLMOD)
// As described in the document, the following alternatives are available:
// - 12-bit (011)
// - 18-bit (110) [DEFAULT]
// - None (111)
#define COLOR_MODE 0b00000101
#define HORIZONTAL_REFRESH 1

// See Table 10.1.1 System Function Command List (1)
//
// These are useful for defining how input data should be displayed. Visual
// descriptions of what each constant does is visible in the documentation.
#define ST7735_MADCTL_MX  0x40 // Column Address Order
#define ST7735_MADCTL_MY  0x80 // Row Address Order
#define ST7735_MADCTL_ML  0x10 // Vertical Refresh Order
#define ST7735_MADCTL_MH  0x04 // Horizontal Refresh Order

// Note: Please use HORIZONTAL_REFRESH, which adjusts the "new" width and
// height accordingly.
#define ST7735_MADCTL_MV  0x20 // Switches rows and columns (90 degree rot.)

// Note: BGR is appropriate for the STM32 and this project's specific pin
// layout, but can be optionally prevented. See the README file for more
// information.
#define ST7735_MADCTL_RGB 0x00 // RGB Color
#define ST7735_MADCTL_BGR 0x08 // BGR Color

// See Table 10.1.1 System Function Command List (1)
#define ST7735_NOP     0x00 // No Operation
#define ST7735_SWRESET 0x01 // Software Reset
#define ST7735_RDDID   0x04 // Display ID
#define ST7735_RDDST   0x09 // Display Status

// See Table 10.1.2 System Function Command List (2)
#define ST7735_SLPIN   0x10 // Unused, Sleep in & booster off
#define ST7735_SLPOUT  0x11 // Sleep out & booster on
#define ST7735_PTLON   0x12 // Partial mode on
#define ST7735_NORON   0x13 // Normal mode on

// See Table 10.1.2 System Function Command List
#define ST7735_INVOFF  0x20 // Invert Colors - Off
#define ST7735_INVON   0x21 // Invert Colors - On
#define ST7735_DISPOFF 0x28 // Display - Off
#define ST7735_DISPON  0x29 // Display - On
#define ST7735_CASET   0x2A // Set Column Address
#define ST7735_RASET   0x2B // Set Row Address
#define ST7735_RAMWR   0x2C // Write to RAM

// See Table 10.1.3 System Function Command List
#define ST7735_COLMOD  0x3A // Color Mode
#define ST7735_MADCTL  0x36 // MADCTL - Control Refresh/Order

/* Color definitions */
// Custom definition for the total sum of colors.
// Useful for demos.
#define COLORS  0x0007

#define	BLACK   0x0000
#define	BLUE    0x001F
#define	RED     0xF800
#define	GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

// Call before initializing any SPI devices
void ST7735_Unselect();

void ST7735_Init(uint8_t rotation);
void ST7735_DrawPixel(uint16_t x, uint16_t y, uint16_t color);
void ST7735_FillRectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color);
void ST7735_FillScreen(uint16_t color);
void ST7735_SetAddressWindow(uint8_t x_high, uint8_t y_high, uint8_t x_low, uint8_t y_low);
void ST7735_ToggleDisplay(bool toggle);
void ST7735_InvertColors(bool invert);

extern int16_t width;
extern int16_t height;
extern int16_t cursor_x;
extern int16_t cursor_y;
extern uint8_t rotation;
extern uint8_t col_start; // offset
extern uint8_t row_start; // offset
extern uint8_t xstart;
extern uint8_t ystart;

#endif // __ST7735_H__
