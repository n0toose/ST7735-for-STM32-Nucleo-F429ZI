/*
 * ST7735.c
 *
 *  Created on: Dec 9, 2022
 *      Author: n0toose
 */

#include <ST7735.h>

int16_t width;
int16_t height;
int16_t cursor_x;
int16_t cursor_y;
uint8_t rotation;
uint8_t col_start;
uint8_t row_start;
uint8_t xstart;
uint8_t ystart;

void ST7735_Select()
{
    HAL_GPIO_WritePin(CS_PORT, CS_PIN, GPIO_PIN_RESET);
}

void ST7735_Unselect()
{
    HAL_GPIO_WritePin(CS_PORT, CS_PIN, GPIO_PIN_SET);
}

void ST7735_Reset()
{
    HAL_GPIO_WritePin(RST_PORT, RST_PIN, GPIO_PIN_RESET);
    HAL_Delay(DELAY);
    HAL_GPIO_WritePin(RST_PORT, RST_PIN, GPIO_PIN_SET);
}

void ST7735_WriteCommand(uint8_t cmd)
{
    HAL_GPIO_WritePin(DC_PORT, DC_PIN, GPIO_PIN_RESET);
    HAL_SPI_Transmit(&ST7735_SPI_PORT, &cmd, sizeof(cmd), HAL_MAX_DELAY);
}

void ST7735_WriteData(uint8_t* buff, size_t buff_size)
{
    HAL_GPIO_WritePin(DC_PORT, DC_PIN, GPIO_PIN_SET);
    HAL_SPI_Transmit(&ST7735_SPI_PORT, buff, buff_size, HAL_MAX_DELAY);
}

// Note: This function assumes that the display has been selected, as the
// function is meant to be used internally.
void DisplayInit()
{
    if (!HORIZONTAL_REFRESH) {
        height = ST7735_HEIGHT;
        width = ST7735_WIDTH;
    } else {
        height = ST7735_WIDTH;
        width = ST7735_HEIGHT;
    }

    xstart = col_start;
    ystart = row_start;

    /* Software Reset */
    ST7735_WriteCommand(ST7735_RASET);
    HAL_Delay(100);

    /* Get Out of Sleep Mode (Redundant) */
    ST7735_WriteCommand(ST7735_SLPOUT);
    HAL_Delay(200);

    /* Columns and Rows */
    // Start: 0
    // End: width - 1, under the assumption that width - 1 is less or equal to
    // a byte. (Each argument is "actually" 2 bytes.)
    ST7735_SetAddressWindow(0, 0, width - 1, height - 1);

    /* Color Mode */

    HAL_Delay(DELAY);

    {
        uint8_t arg = COLOR_MODE;

        ST7735_WriteCommand(ST7735_COLMOD);
        ST7735_WriteData(&arg, 1);
    }

    /* Color Scheme and Rotation */

    // We use BGR in the STM32 platform deliberately, as the colors would
    // otherwise appear inverted with the current pin layout.
    {
         uint8_t madctl = ST7735_MADCTL_MX | ST7735_MADCTL_MY | ST7735_MADCTL_BGR;

         if (HORIZONTAL_REFRESH) {
             madctl |= ST7735_MADCTL_MV;
         }

         ST7735_WriteCommand(ST7735_MADCTL);
         ST7735_WriteData(&madctl, 1);
    }

    /* Enable Display */

    ST7735_WriteCommand(ST7735_NORON);
    ST7735_ToggleDisplay(true);
}

void ST7735_SetAddressWindow(
    uint8_t x_high,
    uint8_t y_high,
    uint8_t x_low,
    uint8_t y_low
) {
    // See Table 10.1.2 System Function Command List
    // Set Column Address
    {
        ST7735_WriteCommand(ST7735_CASET);
        uint8_t column_data[4] = {[1] = x_high + xstart, [3] = x_low + xstart};
        ST7735_WriteData(column_data, sizeof(column_data));
    }

    // Set Row Address
    {
        ST7735_WriteCommand(ST7735_RASET);
        uint8_t row_data[4] = {[1] = y_high + ystart, [3] = y_low + ystart};
        ST7735_WriteData(row_data, sizeof(row_data));
    }

    // Write to RAM
    ST7735_WriteCommand(ST7735_RAMWR);
}

void ST7735_Init(uint8_t rotation)
{
    ST7735_Select();
    ST7735_Reset();
    DisplayInit();

    col_start = 0;
    row_start = 0;

    ST7735_Unselect();
}

void ST7735_DrawPixel(uint16_t x, uint16_t y, uint16_t color) {
    if ((x >= width) || (y >= height)) { // Out of bounds
        return;
    }

    ST7735_Select();

    // Sets "affected" area of screen / cursor.
    ST7735_SetAddressWindow(x, y, x + 1, y + 1);

    uint8_t data[1];

    // Convert color to standard RGB.
    data[0] = color >> 8;
    data[1] = color & 0xFF; // RGB = 8 bits long, 0xFF 'truncates' the color.

    ST7735_WriteData(data, sizeof(data));

    ST7735_Unselect();
}

void ST7735_FillRectangle(
    uint16_t x,
    uint16_t y,
    uint16_t width_rect,
    uint16_t height_rect,
    uint16_t color
) {
    /* Clip Checks */
    if ((x >= width) || (y >= height)) {
    	return;
    }

    if ((x + width_rect - 1) >= width) {
    	width_rect = width - x;
    }

    if ((y + height_rect - 1) >= height) {
    	height_rect = height - y;
    }

    ST7735_Select();
    ST7735_SetAddressWindow(x, y, x + width_rect - 1, y + height_rect - 1);

    uint8_t data[] = { color >> 8, color & 0xFF };
    HAL_GPIO_WritePin(DC_PORT, DC_PIN, GPIO_PIN_SET);

    // TODO: Optimize this using a more efficient algorithm.
    for (y = 0; y < height_rect; y++) {
        for (x = 0; x < width_rect; x++) {
            HAL_SPI_Transmit(&ST7735_SPI_PORT, data, sizeof(data), HAL_MAX_DELAY);
        }
    }

    ST7735_Unselect();
}

void ST7735_FillScreen(uint16_t color) {
    ST7735_FillRectangle(0, 0, width, height, color);
}

// Display (on and off)
void ST7735_ToggleDisplay(bool toggle) {
    ST7735_Select();
    ST7735_WriteCommand(toggle ? ST7735_DISPON : ST7735_DISPOFF);
    ST7735_Unselect();
}

// Display inversion (on and off)
// See Table 10.1.2 System Function command List (2))
void ST7735_InvertColors(bool invert) {
    ST7735_Select();
    ST7735_WriteCommand(invert ? ST7735_INVON : ST7735_INVOFF);
    ST7735_Unselect();
}
