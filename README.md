# ST7735 TFT-Display 1.8 for the STM32

This repository contains the project files and a custom written library that
allow an STM32 Nucleo F429ZI (`NUCLEO-F429ZI`) microcontroller to interact
with an ST7735 display.

**Disclaimer:** This library was developed as a part of a university
course. If you found this repository from a search engine, please *do not*
expect the code contained here to be usable in a production environment.

## Goals

- Keep it simple, silly.
- Provide a very rudimentary set of functions, while keeping it extensible.
- Use a universally accepted naming scheme that makes backwards compatibility
  with other implementations designed for other boards possible.

One of the things that I did not change because of time constraints is
the dependency on constant variables, instead of having a higher degree of
customizability using function parameters. **Contributions welcome!**

### Presumed Specifications

`ST7735.h` contains some preconfigured macros tailored to the use cases of the
author. If you are using a different, driver-compatible display, make sure to
review `ST7735_WIDTH`, `ST7735_HEIGHT` and `COLOR_MODE`, as there is a chance
that your display may not work out of the box.

- Size: 4,57cm (1,8")
- Resolution: 128x160
- Real-life dimensions: 35x28mm
- Contrast: 1:400
- Communication Protocol: SPI
- LED Backlight: Yes
- Driver: `ST7735R`

## Recommended / Preconfigured Pin Layout

| TFT Pin | STM32 Pin        |
|---------|------------------|
| VCC     | **3.3**          |
| GND     | GND              |
| SCL     | PA5 (SCK, SPI_B) |
| SDA     | PB5 (MOSI, SPI_B)|
| RS/DC   | PA6 (GPIOA)      |
| RES     | PD14 (GPIOD)     |
| CS      | PD15 (GPIOD)     |

## Licensing

The files `ST7735.c` and `ST7735.h` are distributed under the terms as
described by the file `LICENSE` (MIT/Expat) in this repository. Other files
present in this repository may be described under different terms, however,
that should be clearly indicated in the files themselves or a file in the
directory.

I consider the code to be a *reimplementation* of the library provided by
[Adafruit for Arduino products](https://github.com/adafruit/Adafruit-ST7735-Library),
rather than a direct derivative. That repository was used as a reference for
the naming convention and some of its code was temporarily used to test other
functions authored by myself. Even though the two projects are meant to have a
certain degree of compatibility with each other (so as to allow demos meant for
the Arduino board to be usable using this library as well), there is no feature
parity or copied code. Depending on your jurisdiction, this could cause legal
complications.

If you wish to incorporate the code in this repository in your project, I
would highly suggest that you credit *Adafruit Industries* as well - without
any guarantees. Both repositories are distributed under the terms of the
MIT/Expat License.
